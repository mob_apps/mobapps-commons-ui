## MobApps UI

Para usar essa lib, você deve efetuar os seguintes passos .

## 1. Add the JitPack repository to your build file
Add it in your root build.gradle at the end of repositories:

```
 allprojects {
	 repositories {
		 ...
		 maven { url 'https://jitpack.io' }
	 }
 }
```


## 2. Add the dependency

```gradle
    	
 dependencies {
    implementation 'org.bitbucket.mob_apps:mobapps-commons-rate:0.0.14'
 }
```
[![](https://jitpack.io/v/org.bitbucket.mob_apps/mobapps-commons-ui.svg)](https://jitpack.io/#org.bitbucket.mob_apps/mobapps-commons-ui)

## 3 Dark Mode

```Theme
...
<!-- values/themes.xml -->
<?xml version="1.0" encoding="utf-8"?>
<resources>
    <style name="Custom.DayNight" parent="MobApps.CommonsUI.DayNight">
        <item name="colorPrimary">@color/color_primary</item>
        <item name="colorPrimaryVariant">@color/color_primary_variant</item>
        <item name="colorSecondary">@color/color_secondary</item>
        <item name="colorSecondaryVariant">@color/color_secondary_variant</item>
    </style>
</resources>

<!-- values-night-v21/themes.xml -->
<resources xmlns:tools="http://schemas.android.com/tools">
    <style name="Custom.DayNight" parent="MobApps.CommonsUI.DayNight">
        <item name="android:statusBarColor">?colorSurface</item>
    </style>
</resources>

<!-- values-night-v23/themes.xml -->
<?xml version="1.0" encoding="utf-8"?>
<resources>
    <style name="Custom.DayNight" parent="MobApps.CommonsUI.DayNight">
        <item name="android:statusBarColor">?colorSurface</item>
        <item name="android:windowLightStatusBar">false</item>
    </style>
</resources>
```

Vocë pode mudar a inicial custom para um nome que represente o app. Ex.: CPF.DayNight ou Placa.NayNight

No seu AndroidManifest.xml use 

```
...
    <application
         ...
        android:theme="@style/Custom.DayNight"
        tools:replace="android:theme"
    />
```

### 3.1 Para permitir a configuração interna

```
...
class MainApp : Application() {
    override fun onCreate() {
        setAppTheme()
    }
}


MobAppsAlertDialog(this).dialogTheme()

```

## 3. Mais exemplos de uso estão nesse link 

https://bitbucket.org/mob_apps/mobapps-commons-ui/src/master/app/src/main/java/com/mobapps/commons/uisamples/


