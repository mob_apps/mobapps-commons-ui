package com.mobapps.commons.uisamples

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.mobapps.commons.mobapps_ktx.exts.initActivity
import com.mobapps.commons.ui.helpers.MobAppsAlertDialog
import com.mobapps.commons.ui.security.ui.activity.SecurityAndPrivacyActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        MobAppsAlertDialog(this).handleDialogIntro()

        btAnotherScreen.setOnClickListener { initActivity<AnotherActivity>() }
        btDialogIntro.setOnClickListener { MobAppsAlertDialog(this).dialogIntro() }
        btPrivacyPolicy.setOnClickListener { initActivity<SecurityAndPrivacyActivity>() }
        btTheme.setOnClickListener { MobAppsAlertDialog(this).dialogTheme() }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {
            R.id.action_ad -> TODO("showIntersticial()")
            R.id.action_about -> initActivity<SecurityAndPrivacyActivity>()
            R.id.action_theme -> MobAppsAlertDialog(this).dialogTheme()
        }

        return super.onOptionsItemSelected(item)
    }

}
