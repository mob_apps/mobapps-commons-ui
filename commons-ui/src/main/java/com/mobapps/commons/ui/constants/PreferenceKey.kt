package com.mobapps.commons.ui.constants

object PreferenceKey {
    const val ALREADY_SHOWN_DIALOG_INTRO = "already_shown_dialog_privacy"
    const val NEED_SHOW_RATE_DIALOG = "need_show_rate_dialog"
    const val THEME = "theme"
}