package com.mobapps.commons.ui.dark_mode.extensions

import android.content.Context
import android.os.Build
import androidx.appcompat.app.AppCompatDelegate
import com.mobapps.commons.mobapps_ktx.helpers.PreferenceHelper

fun Context.setAppTheme(){
    when(PreferenceHelper.defaultPrefs(this).getInt("theme", AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)){
        0 -> AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        1 -> AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        2 -> if (Build.VERSION.SDK_INT >= 28) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)
        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_AUTO_BATTERY)
        }
    }
}