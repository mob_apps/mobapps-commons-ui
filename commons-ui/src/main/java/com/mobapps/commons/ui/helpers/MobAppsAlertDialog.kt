package com.mobapps.commons.ui.helpers

import android.app.Activity
import android.content.SharedPreferences
import android.os.Build
import androidx.appcompat.app.AppCompatDelegate
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.mobapps.commons.ui.constants.PreferenceKey
import com.mobapps.commons.mobapps_ktx.helpers.PreferenceHelper
import com.mobapps.commons.mobapps_ktx.helpers.PreferenceHelper.get
import com.mobapps.commons.mobapps_ktx.helpers.PreferenceHelper.set
import com.mobapps.commons.ui.R
import com.mobapps.commons.ui.security.adapter.SecurityRecyclerAdapter
import com.mobapps.commons.ui.security.model.Security


class MobAppsAlertDialog(private val activity: Activity) {

    private val prefs: SharedPreferences by lazy {
        PreferenceHelper.defaultPrefs(activity)
    }

    private var alreadyShownDialogIntro: Boolean
        get() = prefs[PreferenceKey.ALREADY_SHOWN_DIALOG_INTRO] ?: true
        set(value) {
            prefs[PreferenceKey.ALREADY_SHOWN_DIALOG_INTRO] = value
        }

    private var theme: Int
        get() = prefs[PreferenceKey.THEME, 2] ?: 2
        set(value) {
            prefs[PreferenceKey.THEME] = value
        }


    fun handleDialogIntro() {
        if (alreadyShownDialogIntro.not())
            dialogIntro()
    }

    fun dialogIntro(){
        // set the custom layout
        val customLayout = activity.layoutInflater.inflate(R.layout.common_ui_dialog_intro, null);
        val recyclerView = customLayout.findViewById<RecyclerView>(R.id.rvSecurity)
        recyclerView.adapter = SecurityRecyclerAdapter(activity, Security.list())
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.setHasFixedSize(true)

        val builder = MaterialAlertDialogBuilder(activity)
            .setTitle("Segurança e Privacidade")
            .setView(customLayout)
            .setPositiveButton("Entendi!") { _, _ -> alreadyShownDialogIntro = true }
        val dialog = builder.create()
        try {
            dialog.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun dialogTheme() {
        val arrayThemes = if(Build.VERSION.SDK_INT >= 28){
            arrayOf(
                "Claro",
                "Escuro",
                "Padrão do sistema"
            )
        }
        else{
            arrayOf(
                "Claro",
                "Escuro",
                "Economia de bateria"
            )
        }
        val dialog = MaterialAlertDialogBuilder(activity)
        dialog.apply {
            setTitle("Tema do App")
            setSingleChoiceItems(arrayThemes, theme) { i, item ->
                theme = item
                when (item){
                    0 -> AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
                    1 -> AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
                    2 -> {
                        if(Build.VERSION.SDK_INT >= 28){
                            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)
                        }
                        else{
                            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_AUTO_BATTERY)
                        }
                    }
                }
                i.dismiss()
            }
            show()
        }
    }
}