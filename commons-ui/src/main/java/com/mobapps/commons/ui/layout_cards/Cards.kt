package com.mobapps.commons.ui.layout_cards

import android.app.Activity
import com.mobapps.commons.ui.layout_cards.model.CardItem
import com.igorronner.irinterstitial.init.IRAds
import com.mobapps.commons.ui.R
import com.mobapps.commons.ui.layout_cards.helpers.RemoteConfigCard

internal object Cards {

    enum class CardItemEnum {
        CPF,
        SCORE,
        ADS,
        REMOTE,
        SECURITY_PRIVACY,
        LIMPAR_NOME,
        DUVIDAS_FREQUENTES,
        AVALIAR_APP,
        COMPARTILHAR_APP
    }

    fun list(activity: Activity): List<CardItem> {
        var values = CardItemEnum.values().toMutableList()
        if (IRAds.isPremium(activity))
            values = values.filter { it != CardItemEnum.ADS }.toMutableList()

        if (RemoteConfigCard.isCardEnabled().not())
            values = values.filter { it != CardItemEnum.REMOTE }.toMutableList()

        var list = values.map { cardEnum ->
            when(cardEnum){
                CardItemEnum.CPF -> CardItem(
                        cardEnum,
                        R.drawable.ic_star,
                        R.color.blue500,
                        "Situação cadastral CPF",
                        "Clique aqui para verificar a situação cadastral do seu CPF",
                        "Verificar"
                )
                CardItemEnum.SCORE -> CardItem(
                        cardEnum,
                        R.drawable.ic_share_variant,
                        R.color.green700,
                        "Consultar score de crédito",
                        "Clique aqui e veja agora mesmo seu score de crédito.",
                        "CONSULTAR"
                )
                CardItemEnum.ADS -> CardItem(cardEnum)
                CardItemEnum.REMOTE -> CardItem(
                        cardEnum,
                        title = RemoteConfigCard.getCardTitle(),
                        description = RemoteConfigCard.getCardDescription(),
                        buttonTitle = RemoteConfigCard.getCardBtTitle(),
                        remoteIconUrl = RemoteConfigCard.getCardIcon(),
                        remoteColor = RemoteConfigCard.getCardColor(),
                        remoteActionUrl = RemoteConfigCard.getCardActionUrl()

                )
                CardItemEnum.SECURITY_PRIVACY -> CardItem(
                        cardEnum,
                        R.drawable.ic_shield_lock,
                        R.color.color_primary,
                        "Segurança e Privacidade",
                        "Acesse facilmente nossas Diretrizes de segurança e confira nossas Políticas de privacidade e Termos de uso",
                        "Ver Agora!"
                )
                CardItemEnum.LIMPAR_NOME -> CardItem(
                        cardEnum,
                        R.drawable.ic_star,
                        R.color.red500,
                        "Limpar nome",
                        "Clique aqui para limpar o seu nome.",
                        "Limpar nome"

                )
                CardItemEnum.DUVIDAS_FREQUENTES -> CardItem(
                        cardEnum,
                        R.drawable.ic_star,
                        R.color.orange500,
                        "Dúvidas Frequentes",
                        "Está com dúvida? Clique Aqui",
                        "Dúvidas Frequentes"

                )
                CardItemEnum.AVALIAR_APP -> CardItem(
                        cardEnum,
                        R.drawable.ic_star,
                        R.color.orange500,
                        "Avaliar App!",
                        "Gostou do App? Que tal avaliar nos avaliar com 5 estrelas na loja?", "Avaliar"

                )
                CardItemEnum.COMPARTILHAR_APP -> CardItem(
                        cardEnum,
                        R.drawable.ic_share_variant,
                        R.color.blue500,
                        "Compartilhar o Aplicativo",
                        "Compartilhe com seus amigos e familiares o aplicativo",
                        "Compartilhar"
                )
            }
        }

        return list
    }
}

