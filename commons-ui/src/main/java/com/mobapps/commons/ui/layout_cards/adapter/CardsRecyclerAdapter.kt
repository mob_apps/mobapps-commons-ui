package com.mobapps.commons.ui.layout_cards.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mobapps.commons.ui.layout_cards.Cards
import com.mobapps.commons.ui.R
import com.mobapps.commons.ui.commons.OnRecyclerClickListener
import com.mobapps.commons.ui.layout_cards.model.CardItem
import com.mobapps.commons.ui.layout_cards.vh.CardAdViewHolder
import com.mobapps.commons.ui.layout_cards.vh.CardItemRemoteViewHolder
import com.mobapps.commons.ui.layout_cards.vh.CardItemViewHolder

internal class CardsRecyclerAdapter(
    private val context: Context?,
    private val data: List<CardItem>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var listener: OnCardClickListener? = null
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(context)
        return when(viewType) {
            VIEW_TYPE_ITEM_REMOTE -> CardItemRemoteViewHolder(inflater.inflate(R.layout.common_ui_item_card, viewGroup, false))
            VIEW_TYPE_AD -> CardAdViewHolder(inflater.inflate(R.layout.common_ui_item_ad_card, viewGroup, false))
            else -> CardItemViewHolder(inflater.inflate(R.layout.common_ui_item_card, viewGroup, false))
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = data[position]

        when(holder) {
            is CardItemViewHolder -> holder.bindView(item, listener)
            is CardAdViewHolder -> holder.bindView(item, listener)
            is CardItemRemoteViewHolder -> holder.bindView(item, listener)
        }

        holder.itemView.setOnClickListener {
            listener?.onCardItemClick(item)
        }
    }

    override fun getItemViewType(position: Int): Int {
        val item = data[position]

        return when(item.enum) {
            Cards.CardItemEnum.ADS -> VIEW_TYPE_AD
            Cards.CardItemEnum.REMOTE -> VIEW_TYPE_ITEM_REMOTE
            else -> VIEW_TYPE_ITEM
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun setListener(listener: OnCardClickListener?) {
        this.listener = listener
    }

    interface OnCardClickListener:
        OnRecyclerClickListener {
        fun onCardItemClick(cardItem: CardItem)
    }

    companion object {
        private const val VIEW_TYPE_AD = -1
        private const val VIEW_TYPE_ITEM = 0
        private const val VIEW_TYPE_ITEM_REMOTE = 1
    }
}