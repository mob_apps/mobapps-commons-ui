package com.mobapps.commons.ui.layout_cards.helpers

import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import com.mobapps.commons.ui.BuildConfig
import com.mobapps.commons.ui.R

object RemoteConfigCard {

    private var remoteConfig = FirebaseRemoteConfig.getInstance()

    fun initRemoteConfig(){
        remoteConfig = FirebaseRemoteConfig.getInstance()
        val configSettings = FirebaseRemoteConfigSettings.Builder()
                .setMinimumFetchIntervalInSeconds(if (BuildConfig.DEBUG) 0 else 43200L)
                .build()
        remoteConfig.setConfigSettingsAsync(configSettings)
        remoteConfig.setDefaultsAsync(R.xml.remote_config_card_defaults)
        remoteConfig.fetch()
                .addOnSuccessListener {
                    remoteConfig.activate()
                }
    }

    fun isCardEnabled()= remoteConfig.getBoolean("card_enabled")

    fun getCardTitle(): String {
        var cardTitle = remoteConfig.getString("card_title")
        if (cardTitle.isEmpty())
            cardTitle = "Mobills - Gerenciador Financeiro"

        return cardTitle
    }

    fun getCardDescription(): String {
        var cardDescription = remoteConfig.getString("card_description")
        if (cardDescription.isEmpty())
            cardDescription = "O Mobills é um aplicativo de controle de gastos e finanças pessoais que irá lhe ajudar a alcançar o sucesso financeiro que você sempre sonhou."

        return cardDescription
    }


    fun getCardIcon(): String {
        var cardIcon = remoteConfig.getString("card_icon")
        if (cardIcon.isEmpty())
            cardIcon = "https://firebasestorage.googleapis.com/v0/b/apps-2870d.appspot.com/o/assets%2Fic_mobills.png?alt=media&token=42bd95bd-39db-4bf3-93d8-72f96cd99323"

        return cardIcon
    }

    fun getCardColor(): String {
        var cardColor = remoteConfig.getString("card_bt_text_color")
        if (cardColor.isEmpty())
            cardColor = "#2196F3"

        return cardColor
    }

    fun getCardBtTitle(): String {
        var cardBtTitle = remoteConfig.getString("card_bt_text")
        if (cardBtTitle.isEmpty())
            cardBtTitle = "Baixar Agora"

        return cardBtTitle
    }

    fun getCardActionUrl(): String {
        var cardActionUrl = remoteConfig.getString("card_action_url")
        if (cardActionUrl.isEmpty())
            cardActionUrl = "https://play.google.com/store/apps/details?id=br.com.gerenciadorfinanceiro.controller"

        return cardActionUrl
    }
}