package com.mobapps.commons.ui.layout_cards.model

import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.IdRes

data class CardItem(
    val enum: Enum<*>? = null,
    @DrawableRes val icon: Int = 0,
    @ColorRes val color: Int = 0,
    val title: String = "",
    val description: String = "",
    val buttonTitle: String = "",
    val isRemote: Boolean = false,
    val remoteIconUrl: String = "",
    val remoteColor: String = "",
    val remoteActionUrl: String = ""
)