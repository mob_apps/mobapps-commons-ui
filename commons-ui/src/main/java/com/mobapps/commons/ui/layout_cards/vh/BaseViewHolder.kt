package com.mobapps.commons.ui.layout_cards.vh

import android.content.Context
import android.content.res.Resources
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.mobapps.commons.ui.commons.OnRecyclerClickListener

internal open class BaseViewHolder<T>(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val context: Context = itemView.context
    val res: Resources = context.resources
    open fun bindView(item: T, listener: OnRecyclerClickListener? = null) = Unit
}