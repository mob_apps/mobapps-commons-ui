package com.mobapps.commons.ui.layout_cards.vh

import android.view.View
import com.google.android.gms.ads.formats.UnifiedNativeAdView
import com.igorronner.irinterstitial.init.IRBanner
import com.mobapps.commons.ui.R
import com.mobapps.commons.ui.commons.OnRecyclerClickListener
import com.mobapps.commons.ui.layout_cards.model.CardItem
import kotlinx.android.synthetic.main.common_ui_item_ad_card.view.*

internal class CardAdViewHolder(itemView: View) :
    BaseViewHolder<CardItem>(itemView) {

    override fun bindView(item: CardItem, listener: OnRecyclerClickListener?) {
        super.bindView(item, listener)
        val adViewNative = itemView.findViewById<UnifiedNativeAdView>(R.id.adViewNative)
        IRBanner.loadNativeAd(adViewNative, true, itemView.adCard)
    }
}