package com.mobapps.commons.ui.layout_cards.vh

import android.annotation.SuppressLint
import android.graphics.Color
import android.view.View
import com.mobapps.commons.ui.commons.OnRecyclerClickListener
import com.mobapps.commons.ui.layout_cards.adapter.CardsRecyclerAdapter
import com.mobapps.commons.ui.layout_cards.model.CardItem
import kotlinx.android.synthetic.main.common_ui_item_card.view.*

internal class CardItemRemoteViewHolder(itemView: View) :
    BaseViewHolder<CardItem>(itemView) {

    @SuppressLint("Range")
    override fun bindView(item: CardItem, listener: OnRecyclerClickListener?) {
        super.bindView(item, listener)
        val color = Color.parseColor(item.remoteColor)

        //itemView.ivIcon.load(item.remoteIconUrl)

        itemView.tvTitle.text = item.title
        itemView.tvDescription.text = item.description
        itemView.btnAction.text = item.buttonTitle
        itemView.btnAction.setTextColor(color)
        itemView.btnAction.setOnClickListener {
            if (listener is CardsRecyclerAdapter.OnCardClickListener)
                listener.onCardItemClick(item)
        }
    }
}