package com.mobapps.commons.ui.layout_cards.vh

import android.graphics.PorterDuff
import android.view.View
import androidx.core.content.ContextCompat
import com.mobapps.commons.ui.commons.OnRecyclerClickListener
import com.mobapps.commons.ui.layout_cards.adapter.CardsRecyclerAdapter
import com.mobapps.commons.ui.layout_cards.model.CardItem
import kotlinx.android.synthetic.main.common_ui_item_card.view.*

internal class CardItemViewHolder(itemView: View) :
    BaseViewHolder<CardItem>(itemView) {

    override fun bindView(item: CardItem, listener: OnRecyclerClickListener?) {
        super.bindView(item, listener)
        val context = itemView.context
        val (_, icon, color1, title, description, buttonTitle) = item
        val color = ContextCompat.getColor(context, color1)
        itemView.ivIcon.setImageResource(icon)
        itemView.ivIcon.setColorFilter(color, PorterDuff.Mode.SRC_IN);
        itemView.tvTitle.text = title
        itemView.tvDescription.text = description
        itemView.btnAction.text = buttonTitle
        itemView.btnAction.setTextColor(color)
        itemView.btnAction.setOnClickListener {
            if (listener is CardsRecyclerAdapter.OnCardClickListener)
                listener.onCardItemClick(item)
        }
    }
}