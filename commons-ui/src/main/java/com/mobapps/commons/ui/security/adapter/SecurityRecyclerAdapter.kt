package com.mobapps.commons.ui.security.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mobapps.commons.ui.R
import com.mobapps.commons.ui.security.model.SecurityItem
import com.mobapps.commons.ui.security.vh.SecurityItemViewHolder

class SecurityRecyclerAdapter(
    private val context: Context?,
    private val data: List<SecurityItem>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(context)
        return SecurityItemViewHolder(
            inflater.inflate(
                R.layout.common_ui_security_item,
                viewGroup,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = data[position]
        if (holder is SecurityItemViewHolder)
            holder.bindView(item)
    }


    override fun getItemCount(): Int {
        return data.size
    }
}