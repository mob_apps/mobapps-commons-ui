package com.mobapps.commons.ui.security.model

import com.mobapps.commons.ui.R

object Security {

    fun list(): MutableList<SecurityItem>{
        val list = mutableListOf<SecurityItem>()
        list.add(SecurityItem(R.drawable.ic_account_lock, "Todos os dados são criptografados e somente você tem acesso a eles"))
        list.add(SecurityItem(R.drawable.ic_server_security, "Não armazenamos NENHUM dado. Tudo ficará disponível apenas no seu dispositivo"))
        list.add(SecurityItem(R.drawable.ic_security_network, "Nosso intuito com este app é trazer até você informação segura e de qualidade"))
        list.add(SecurityItem(R.drawable.ic_shield_lock, "Todas informações do Aplicativo vêm de fontes e serviços confiáveis. Informação válida é nosso compromisso."))
        return list
    }
}