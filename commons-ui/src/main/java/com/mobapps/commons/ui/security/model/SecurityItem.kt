package com.mobapps.commons.ui.security.model

import androidx.annotation.DrawableRes

data class SecurityItem(@DrawableRes val icon: Int, val text: String)