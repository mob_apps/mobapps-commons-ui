package com.mobapps.commons.ui.security.ui.activity

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.mobapps.commons.mobapps_ktx.exts.initActivity
import com.mobapps.commons.ui.R
import com.mobapps.commons.ui.security.adapter.SecurityRecyclerAdapter
import com.mobapps.commons.ui.security.model.Security
import kotlinx.android.synthetic.main.activity_security_and_privacy.*
import kotlinx.android.synthetic.main.common_ui_app_bar.*

class SecurityAndPrivacyActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_security_and_privacy)
        setSupportActionBar(toolbar)
        supportActionBar?.title = "Segurança e Privacidade"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        rvSecurity.adapter = SecurityRecyclerAdapter(this, Security.list())
        rvSecurity.layoutManager = LinearLayoutManager(this)
        rvSecurity.setHasFixedSize(true)
        btPrivacyPolicy.setOnClickListener { initActivity<PrivacyPolicyActivity> () }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return true
    }
}
