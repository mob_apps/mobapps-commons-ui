package com.mobapps.commons.ui.security.vh

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.mobapps.commons.ui.security.model.SecurityItem
import kotlinx.android.synthetic.main.common_ui_security_item.view.*

class SecurityItemViewHolder(itemView: View) :
    RecyclerView.ViewHolder(itemView) {

    fun bindView(item: SecurityItem) {
        itemView.ivImg.setImageResource(item.icon)
        itemView.tvTitle.text = item.text
    }

}